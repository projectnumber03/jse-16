import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.context.Bootstrap;

import java.lang.Exception;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TaskOperationTest {

    class TaskFactory {

        private int count = 1;

        private TaskDTO createTask() {
            @NotNull final TaskDTO p = new TaskDTO();
            p.setName("task" + count);
            p.setDescription("description" + count);
            p.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
            p.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(1)));
            p.setStatus(Status.PLANNED);
            count++;
            return p;
        }

        private void createTasks(@NotNull final Bootstrap b, final int count) throws Exception {
            for (int i = 0; i < count; i++) {
                b.getTaskEndPoint().persistTask(b.getToken(), createTask());
            }
        }

    }

    @Before
    public void registerUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        @NotNull final UserDTO testUser = new UserDTO();
        testUser.setLogin("test");
        testUser.setPassword("test");
        testUser.setRole(Role.ADMIN);
        b.getUserEndPoint().registerUser(testUser);
    }

    @After
    public void deleteUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        @Nullable final String idToDelete = b.getUserEndPoint().findAllUsers(b.getToken()).stream()
                .filter(u -> "test".equals(u.getLogin()))
                .map(UserDTO::getId).findAny().orElse(null);
        b.getUserEndPoint().removeOneUserById(b.getToken(), idToDelete);
    }

    @Test
    public void clearTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        tf.createTasks(b, 10);
        @NotNull List<TaskDTO> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(10, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(0, tasks.size());
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @NotNull List<TaskDTO> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(10, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void findByNameOrDescriptionTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @NotNull List<TaskDTO> tasks = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task1");
        assertEquals(2, tasks.size());
        tasks = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "description1");
        assertEquals(2, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void findOneTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @Nullable final TaskDTO taskToFind = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task2").stream().findAny().orElse(null);
        assertNotNull(taskToFind);
        @NotNull final TaskDTO task = b.getTaskEndPoint().findOneTask(b.getToken(), taskToFind.getId());
        assertEquals(task.getId(), taskToFind.getId());
        assertEquals(task.getName(), taskToFind.getName());
        assertEquals(task.getDescription(), taskToFind.getDescription());
        assertEquals(task.getStart(), taskToFind.getStart());
        assertEquals(task.getFinish(), taskToFind.getFinish());
        assertEquals(task.getUserId(), taskToFind.getUserId());
        assertEquals(task.getStatus(), taskToFind.getStatus());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void mergeTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        tf.createTasks(b, 10);
        @Nullable final TaskDTO taskToMerge = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task1").stream().findAny().orElse(null);
        assertNotNull(taskToMerge);
        taskToMerge.setName("task-1");
        taskToMerge.setDescription("description-1");
        taskToMerge.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(2)));
        taskToMerge.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(3)));
        taskToMerge.setStatus(Status.IN_PROCESS);
        b.getTaskEndPoint().mergeTask(b.getToken(), taskToMerge);
        @NotNull final List<TaskDTO> empty = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "test-task1");
        assertEquals(0, empty.size());
        @Nullable final TaskDTO task = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task-1").stream().findAny().orElse(null);
        assertNotNull(task);
        assertEquals(task.getName(), "task-1");
        assertEquals(task.getDescription(), "description-1");
        assertEquals(task.getStart(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(2)));
        assertEquals(task.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(3)));
        assertEquals(task.getStatus(), Status.IN_PROCESS);
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void persistTest() throws Exception {
        @NotNull final TaskFactory tf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        b.getTaskEndPoint().persistTask(b.getToken(), tf.createTask());
        @Nullable final TaskDTO task = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task1").stream().findAny().orElse(null);
        assertNotNull(task);
        assertEquals(task.getName(), "task1");
        assertEquals(task.getDescription(), "description1");
        assertEquals(task.getStart(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
        assertEquals(task.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(1)));
        assertEquals(task.getStatus(), Status.PLANNED);
        @NotNull final List<TaskDTO> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(1, tasks.size());
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

    @Test
    public void removeTest() throws Exception {
        @NotNull final TaskFactory pf = new TaskFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
        pf.createTasks(b, 10);
        @Nullable final TaskDTO taskToDelete = b.getTaskEndPoint().findTasksByNameOrDescription(b.getToken(), "task2").stream().findAny().orElse(null);
        assertNotNull(taskToDelete);
        b.getTaskEndPoint().removeOneTaskByUserId(b.getToken(), taskToDelete.getId());
        @NotNull final List<TaskDTO> tasks = b.getTaskEndPoint().findTasksByUserId(b.getToken());
        assertEquals(9, tasks.size());
        assertFalse(tasks.stream().map(TaskDTO::getName).anyMatch("task2"::equals));
        b.getTaskEndPoint().removeTasksByUserId(b.getToken());
    }

}
