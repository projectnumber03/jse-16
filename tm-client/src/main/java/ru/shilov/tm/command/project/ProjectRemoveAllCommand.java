package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class ProjectRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getEndPointLocator().getProjectEndPoint().removeAllProjects(getToken());
        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Полное удаление всех проектов";
    }

}
