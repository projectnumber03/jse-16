package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class TaskRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        getEndPointLocator().getTaskEndPoint().removeAllTasks(getToken());
        System.out.println("[ВСЕ ЗАДАЧИ УДАЛЕНЫ]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-all";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Полное удаление всех задач";
    }

}
