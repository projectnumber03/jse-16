package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class UserLoginCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        @NotNull String login = getServiceLocator().getTerminalService().nextLine();
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        @NotNull String password = getServiceLocator().getTerminalService().nextLine();
        @Nullable String token = getEndPointLocator().getSessionEndPoint().createSession(login, password);
        while(token == null) {
            System.out.println("НЕВЕРНЫЙ ЛОГИН/ПАРОЛЬ");
            System.out.println("ВВЕДИТЕ ЛОГИН:");
            login = getServiceLocator().getTerminalService().nextLine();
            System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
            password = getServiceLocator().getTerminalService().nextLine();
            token = getEndPointLocator().getSessionEndPoint().createSession(login, password);
        }
        getSessionLocator().setToken(token);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

}
