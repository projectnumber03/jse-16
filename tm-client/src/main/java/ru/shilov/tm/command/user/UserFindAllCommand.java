package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.UserDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class UserFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<UserDTO> users = getEndPointLocator().getUserEndPoint().findAllUsers(getToken());
        getServiceLocator().getTerminalService().printAllUsers(users);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список пользователей";
    }

}
