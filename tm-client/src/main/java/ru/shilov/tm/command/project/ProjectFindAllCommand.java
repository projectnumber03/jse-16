package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.ProjectDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<ProjectDTO> projects = getEndPointLocator().getProjectEndPoint().findProjectsByUserId(getToken());
        getServiceLocator().getTerminalService().printAllProjects(projects);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список проектов";
    }

}
