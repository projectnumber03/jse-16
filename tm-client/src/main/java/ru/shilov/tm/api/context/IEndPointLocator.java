package ru.shilov.tm.api.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.api.service.ITerminalService;

public interface IEndPointLocator {

    @NotNull
    ISessionEndPoint getSessionEndPoint();

    @NotNull
    IProjectEndPoint getProjectEndPoint();

    @NotNull
    ITaskEndPoint getTaskEndPoint();

    @NotNull
    IUserEndPoint getUserEndPoint();

}
