package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IUserEndPoint;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.dto.UserDTO;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IUserEndPoint")
public final class UserEndPointImpl implements IUserEndPoint {

    @NotNull
    private Bootstrap bootstrap;
    
    @NotNull
    @Override
    public List<UserDTO> findAllUsers(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        return bootstrap.getUserService().findAll().stream().map(User::toUserDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public UserDTO findOneUser(@Nullable final String token) throws Exception {
        @NotNull final Session currentSession = bootstrap.getSessionService().validate(token);
        return User.toUserDTO(bootstrap.getUserService().findOne(currentSession.getUser().getId()));
    }

    @NotNull
    @Override
    public UserDTO findOneUserById(@Nullable final String token, @Nullable final String userId) throws Exception {
        bootstrap.getSessionService().validate(token);
        return User.toUserDTO(bootstrap.getUserService().findOne(userId));
    }

    @Override
    public void removeAllUsers(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        bootstrap.getUserService().removeAll();
    }

    @NotNull
    @Override
    public UserDTO persistUser(@Nullable final String token, @Nullable final UserDTO u) throws Exception {
        bootstrap.getSessionService().validate(token);
        bootstrap.getUserService().persist(UserDTO.toUser(bootstrap, u));
        return u;
    }

    @NotNull
    @Override
    public UserDTO mergeUser(@Nullable final String token, @NotNull final UserDTO u) throws Exception {
        bootstrap.getSessionService().validate(token);
        bootstrap.getUserService().merge(UserDTO.toUser(bootstrap, u));
        return u;
    }

    @Override
    public void removeOneUserById(@Nullable final String token, @Nullable final String id) throws Exception {
        bootstrap.getSessionService().validate(token, Arrays.asList(User.Role.ADMIN));
        bootstrap.getUserService().removeOneById(id);
    }

    @NotNull
    @Override
    public UserDTO registerUser(@Nullable final UserDTO u) {
        bootstrap.getUserService().register(UserDTO.toUser(bootstrap, u));
        return u;
    }

}
