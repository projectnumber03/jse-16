package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IProjectEndPoint;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.dto.ProjectDTO;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IProjectEndPoint")
public final class ProjectEndPointImpl implements IProjectEndPoint {

    @NotNull
    private Bootstrap bootstrap;

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        return bootstrap.getProjectService().findAll().stream().map(Project::toProjectDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllProjectsOrderByFinish(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        return bootstrap.getProjectService()
                .findByUserIdOrderBy(bootstrap.getSessionService().validate(token).getUser().getId(), "finish")
                .stream().map(Project::toProjectDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllProjectsOrderByStart(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        return bootstrap.getProjectService()
                .findByUserIdOrderBy(bootstrap.getSessionService().validate(token).getUser().getId(), "start")
                .stream().map(Project::toProjectDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllProjectsOrderByStatus(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        return bootstrap.getProjectService()
                .findByUserIdOrderBy(bootstrap.getSessionService().validate(token).getUser().getId(), "status")
                .stream().map(Project::toProjectDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findOneProject(@Nullable final String session, @NotNull final String id) throws Exception {
        bootstrap.getSessionService().validate(session);
        return Project.toProjectDTO(bootstrap.getProjectService().findOne(id));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findProjectsByUserId(@Nullable final String session) throws Exception {
        return bootstrap.getProjectService().findByUserId(bootstrap.getSessionService().validate(session).getUser().getId()).stream().map(Project::toProjectDTO).collect(Collectors.toList());
    }

    @Override
    @WebMethod
    public void removeAllProjects(@Nullable final String session) throws Exception {
        bootstrap.getSessionService().validate(session, Arrays.asList(User.Role.ADMIN));
        bootstrap.getProjectService().removeAll();
    }

    @Override
    public void removeProjectsByUserId(@Nullable final String session) throws Exception {
        bootstrap.getProjectService().removeByUserId(bootstrap.getSessionService().validate(session).getUser().getId());
    }

    @Override
    public void removeOneProjectByUserId(@Nullable final String session, @Nullable final String id) throws Exception {
        bootstrap.getProjectService().removeOneByUserId(bootstrap.getSessionService().validate(session).getUser().getId(), id);
    }

    @NotNull
    @Override
    public ProjectDTO persistProject(@Nullable final String session, @NotNull final ProjectDTO p) throws Exception {
        @NotNull final Session currentSession = bootstrap.getSessionService().validate(session);
        p.setUserId(currentSession.getUser().getId());
        bootstrap.getProjectService().persist(ProjectDTO.toProject(bootstrap, p));
        return p;
    }

    @NotNull
    @Override
    public ProjectDTO mergeProject(@Nullable final String session, @NotNull final ProjectDTO p) throws Exception {
        bootstrap.getSessionService().validate(session);
        bootstrap.getProjectService().merge(ProjectDTO.toProject(bootstrap, p));
        return p;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findProjectsByNameOrDescription(@Nullable final String session, @NotNull final String value) throws Exception {
        return bootstrap.getProjectService()
                .findByNameOrDescription(bootstrap.getSessionService().validate(session).getUser().getId(), value)
                .stream().map(Project::toProjectDTO).collect(Collectors.toList());
    }

}
