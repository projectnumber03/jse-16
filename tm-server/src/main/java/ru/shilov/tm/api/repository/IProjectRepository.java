package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) throws SQLException;

    void removeByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    List<Project> findByUserId(@NotNull final String userId) throws SQLException;

    void removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @NotNull
    @Override
    List<Project> findAll();

    @Nullable
    @Override
    Project findOne(@NotNull final String id);

    @Override
    void removeAll();

    @Override
    void persist(@NotNull final Project entity);

    @Override
    void merge(@NotNull final Project entity);

    @NotNull
    List<Project> findByUserIdOrderBy(@NotNull final String userId, @NotNull final String field);

}
