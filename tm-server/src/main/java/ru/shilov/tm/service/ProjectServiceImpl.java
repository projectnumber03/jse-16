package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.repository.ProjectRepositoryImpl;

import javax.persistence.EntityManager;
import java.util.List;

@SuperBuilder
public final class ProjectServiceImpl extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Override
    public List<Project> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return ProjectRepositoryImpl.builder().entityManager(em).build().findByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@Nullable final String userId, @Nullable final String value) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return ProjectRepositoryImpl.builder().entityManager(em).build().findByNameOrDescription(userId, value);
    }

    @Override
    public void removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        ProjectRepositoryImpl.builder().entityManager(em).build().removeByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        ProjectRepositoryImpl.builder().entityManager(em).build().removeOneByUserId(userId, id);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Project> findByUserIdOrderBy(@NotNull final String userId, @NotNull final String field) {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(field)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return ProjectRepositoryImpl.builder().entityManager(em).build().findByUserIdOrderBy(userId, field);
    }

    @Override
    public IRepository<Project> getRepository(@NotNull final EntityManager em) {
        return ProjectRepositoryImpl.builder().entityManager(em).build();
    }

}
