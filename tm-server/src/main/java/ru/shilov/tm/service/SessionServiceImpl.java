package ru.shilov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.dto.SessionDTO;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.repository.SessionRepositoryImpl;
import ru.shilov.tm.util.AES;
import ru.shilov.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@SuperBuilder
public final class SessionServiceImpl extends AbstractService<Session> implements ISessionService {

    @Override
    public void removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        SessionRepositoryImpl.builder().entityManager(em).build().removeByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String userId, @Nullable final String id) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        @NotNull final EntityManager em = bootstrap.getEntityManagerFactory().createEntityManager();
        return SessionRepositoryImpl.builder().entityManager(em).build().findOneByUserId(userId, id);
    }

    @Nullable
    public Session create(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final User user = bootstrap.getUserService().findByLogin(login);
        @Nullable final String salt = bootstrap.getSettingService().getProperty("salt");
        @Nullable final Integer cycle = Integer.parseInt(bootstrap.getSettingService().getProperty("cycle"));
        if(user.getPassword() != null && !user.getPassword().equals(SignatureUtil.sign(password, salt, cycle))) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(Session.toSessionDTO(session), salt, cycle));
        persist(session);
        return session;
    }

    @NotNull
    @Override
    public Session validate(@Nullable final String sessionToValidate, @NotNull final List<User.Role> roles) throws Exception {
        if (sessionToValidate == null) throw new PermissionException();
        @NotNull final Session userSesstion = decrypt(sessionToValidate);
        if (!roles.contains(userSesstion.getRole())) throw new PermissionException();
        return validate(sessionToValidate);
    }

    @NotNull
    @Override
    public Session validate(@Nullable final String sessionToValidate) throws Exception {
        if (sessionToValidate == null) throw new PermissionException();
        @NotNull final Session userSession = decrypt(sessionToValidate);
        @Nullable final Session serverSession = findOneByUserId(userSession.getUser().getId(), userSession.getId());
        if (serverSession == null) throw new NoSuchSessionException();
        @Nullable final String salt = bootstrap.getSettingService().getProperty("salt");
        @Nullable final Integer cycle = Integer.parseInt(bootstrap.getSettingService().getProperty("cycle"));
        if (serverSession.getSignature() != null && !serverSession.getSignature().equals(SignatureUtil.sign(Session.toSessionDTO(userSession), salt, cycle))) throw new IllegalSessionException();
        if (Duration.between(serverSession.getCreationDate(), LocalDateTime.now()).getSeconds() > 90) throw new SessionTimeOutException();
        return userSession;
    }

    @Override
    public Session decrypt(@Nullable final String cryptSession) throws Exception {
        @NotNull final String key = bootstrap.getSettingService().getProperty("decryption.key");
        @NotNull final String json = AES.decrypt(cryptSession, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final SessionDTO session = mapper.readValue(json, SessionDTO.class);
        return SessionDTO.toSession(bootstrap, session);
    }

    @Override
    public IRepository<Session> getRepository(@NotNull final EntityManager em) {
        return SessionRepositoryImpl.builder().entityManager(em).build();
    }

}
