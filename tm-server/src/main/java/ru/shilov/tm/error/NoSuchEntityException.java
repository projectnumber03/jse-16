package ru.shilov.tm.error;

import org.jetbrains.annotations.NotNull;

public final class NoSuchEntityException extends RuntimeException {

    public NoSuchEntityException() {
        super("Ошибка получения объекта");
    }

    public NoSuchEntityException(@NotNull final Throwable throwable) {
        super("Ошибка получения объекта", throwable);
    }

}
