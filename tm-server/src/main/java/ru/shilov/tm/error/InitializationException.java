package ru.shilov.tm.error;

public final class InitializationException extends RuntimeException {

    public InitializationException() {
        super("Ошибка инициализации");
    }

}
