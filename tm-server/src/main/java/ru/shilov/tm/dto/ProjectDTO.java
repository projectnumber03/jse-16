package ru.shilov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.enumerated.Status;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    private static final long SerialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String start;

    @NotNull
    private String finish;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    public static Project toProject(@NotNull final Bootstrap bootstrap, @NotNull final ProjectDTO projectDTO) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setUser(bootstrap.getUserService().findOne(projectDTO.getUserId()));
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStart(LocalDate.parse(projectDTO.getStart(), DateTimeFormatter.ISO_LOCAL_DATE));
        project.setFinish(LocalDate.parse(projectDTO.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE));
        project.setStatus(projectDTO.getStatus());
        project.setTasks(bootstrap.getTaskService().findByProjectId(projectDTO.getUserId(), projectDTO.getId()));
        return project;
    }

}